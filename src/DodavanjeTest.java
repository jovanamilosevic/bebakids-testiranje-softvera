// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
//import org.hamcrest.Matcher;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class DodavanjeTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Jovana\\Downloads\\chromedriver.exe");
	driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void test() {
	  driver.get("https://www.bebakids.com/");
	    driver.manage().window().setSize(new Dimension(1382, 744));
	    driver.findElement(By.cssSelector(".cookie-agree-gdpr")).click();
	    driver.findElement(By.cssSelector(".header-carthor-total")).click();
	    driver.findElement(By.cssSelector(".empty-cart-alert-wrapper > .btn")).click();
	    js.executeScript("window.scrollTo(0,200)");
	    {
	        WebElement element = driver.findElement(By.cssSelector(".wrapper-grid-view:nth-child(1) .img-hover .img-responsive"));
	        Actions builder = new Actions(driver);
	        builder.moveToElement(element).perform();
	      }
	    driver.findElement(By.cssSelector(".wrapper-grid-view:nth-child(1) .img-hover .img-responsive")).click();
	    js.executeScript("window.scrollTo(0,199)");
	    driver.findElement(By.cssSelector(".product-attributes > .ease:nth-child(1)")).click();
	    driver.findElement(By.id("nb_addToCartButton")).click();
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    {
      WebElement element = driver.findElement(By.cssSelector(".slider_recommend_decaci > a > span"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    
    driver.findElement(By.linkText("Jakne")).click();
    js.executeScript("window.scrollTo(0,201)");
    {
        WebElement element = driver.findElement(By.cssSelector(".wrapper-grid-view:nth-child(1) .img-hover .img-responsive"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
      }
    driver.findElement(By.cssSelector(".wrapper-grid-view:nth-child(1) .img-hover .img-responsive")).click();
    js.executeScript("window.scrollTo(0,200)");

    driver.findElement(By.cssSelector(".product-attributes > .ease:nth-child(1)")).click();
    driver.findElement(By.id("nb_addToCartButton")).click();
    {
      WebElement element = driver.findElement(By.cssSelector(".slider_recommend_girls > a > span"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    driver.findElement(By.linkText("Duksevi")).click();
    js.executeScript("window.scrollTo(0,202)");
    {
        WebElement element = driver.findElement(By.cssSelector(".wrapper-grid-view:nth-child(1) .img-hover .img-responsive"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
      }
    driver.findElement(By.cssSelector(".wrapper-grid-view:nth-child(1) .img-hover .img-responsive")).click();
    js.executeScript("window.scrollTo(0,200)");

    driver.findElement(By.cssSelector(".product-attributes > .ease:nth-child(1)")).click();
    driver.findElement(By.id("nb_addToCartButton")).click();
    driver.findElement(By.cssSelector(".header-carthor-total")).click();
    js.executeScript("window.scrollTo(0,200)");
    
    // Provera da li su razlicite kategorije
    String kategorija1 = driver.findElement(By.linkText("BEBE")).getText();
	String kategorija2 = driver.findElement(By.linkText("DEVOJ�ICE")).getText();
	String kategorija3 = driver.findElement(By.linkText("DE�ACI")).getText();

	assertNotEquals(kategorija1, kategorija2);
	assertNotEquals(kategorija2, kategorija3);
	assertNotEquals(kategorija1, kategorija3);
	
	// Provera da li ima tri elementa u korpi
		assertEquals(driver.findElement(By.cssSelector(".header-carthor-total")).getText(), "3");
    
    
    //Provera cene
		String cena1proizvoda=driver.findElement(By.cssSelector(".item:nth-child(1) .product-item-prices")).getText();
		String cena1=cena1proizvoda.substring(0,3);
		double c1=Double.parseDouble(cena1);
		
		
		String cena2proizvoda=driver.findElement(By.cssSelector(".item:nth-child(3) .product-item-prices")).getText();
		String cena2=cena2proizvoda.substring(0, 3);
		double c2=Double.parseDouble(cena2);
		
		
		String cena3proizvoda=driver.findElement(By.cssSelector(".item:nth-child(2) .product-item-prices")).getText();
		String cena3bezTacke=cena3proizvoda.replace(".", "");
		String cena3=cena3bezTacke.substring(0, 4);
		double c3=Double.parseDouble(cena3);
		
		
		String stvarnaCenaProizvoda=driver.findElement(By.cssSelector(".text-right:nth-child(6)")).getText();
		String sC=stvarnaCenaProizvoda.replace(".", "");
		String sCena=sC.substring(0,4);
		double stvarnaCena=Double.parseDouble(sCena);
		
		double ocekivanaCena=c1+c2+c3;
		assertEquals(ocekivanaCena, stvarnaCena,0.001);
		
		
		

  }

}
