import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


public class TestRunner {

	public static void main(String[] args) throws SecurityException, IOException {
		
		Result result = JUnitCore.runClasses(RegistracijaTest.class);
		Logger l =  Logger.getLogger("tekst-report.txt");
		
		
		for (Failure f: result.getFailures()) {
			l.warning(f.toString());
		}
		l.info("RegiistracijaTest: Proces registrovanja korisnika, "+result.wasSuccessful());
		
		result = JUnitCore.runClasses(PrijavaTest.class);
		 l.info("PrijavaTest: Proces prijave korisnika, "+result.wasSuccessful());
		 
		 result = JUnitCore.runClasses(ProveraPodatakaTest.class);
		 l.info("ProveraPodatakaTest: Uporedjuje podatje pri registraciji sa podacima na nalogu, "+result.wasSuccessful());
		 
		 result = JUnitCore.runClasses(DodavanjeTest.class);
		 l.info("DodavanjeTest: Dodavanje 3 proizvodau korpu iz razlicitih kategorija, "+result.wasSuccessful());
		 
		 result = JUnitCore.runClasses(DodatnoTest.class);
		 l.info("DodatnoTest: provera naslova i URL stranice"+result.wasSuccessful());
		 
		 result = JUnitCore.runClasses(KomentarTest.class);
		 l.info("KomentarTest: Dodaje komentar na aktuelan katalog, "+result.wasSuccessful());
		 
		 result = JUnitCore.runClasses(KompanijaPodaciTest.class);
		 l.info("DKompanijaPodaciTest: kupi podatke o kompaniji"+result.wasSuccessful());
		 
		 result = JUnitCore.runClasses(PostaniVIPTest.class);
		 l.info("PostaniVIPTest: salje zahtev da postane vip korisnik, "+result.wasSuccessful());
		 

	}

}